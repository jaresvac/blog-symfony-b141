<?php

namespace Cvut\Fit\BiWt1\Blog\CommonBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CommentType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('text', null, [
				'required' => true
			])
			->add('files', 'collection', [
				'type' => new FileType(),
				'allow_add' => true,
				'allow_delete' => true,
				'delete_empty' => true,
				'prototype' => true,
				// Post update
				'by_reference' => false,
			])
			->add('submit', 'submit', [
				'attr' => [
					'class' => 'btn btn-primary'
				]
			]);
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Cvut\Fit\BiWt1\Blog\CommonBundle\Entity\Comment'
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'cvut_fit_biwt1_blog_commonbundle_comment';
	}
}
