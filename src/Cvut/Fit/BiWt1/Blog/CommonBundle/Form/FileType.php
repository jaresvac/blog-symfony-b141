<?php

namespace Cvut\Fit\BiWt1\Blog\CommonBundle\Form;

use Cvut\Fit\BiWt1\Blog\CommonBundle\Entity\Image;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FileType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('name')
			->add('data', 'file', [
				'required' => false,
				'data_class' => NULL,
			]);

		$builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
			$file = $event->getData();

			if ($file->getData() instanceof UploadedFile) {

				/** @var $data UploadedFile */
				$file->setInternetMediaType($file->getData()->getMimeType());
				//var_dump(file_get_contents((string)$file->getData()));
				//exit;
				$file->setData(file_get_contents((string)$file->getData()));
			}
		});
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Cvut\Fit\BiWt1\Blog\CommonBundle\Entity\File'
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'cvut_fit_biwt1_blog_commonbundle_file';
	}
}
