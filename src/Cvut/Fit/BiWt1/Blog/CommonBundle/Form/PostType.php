<?php

namespace Cvut\Fit\BiWt1\Blog\CommonBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PostType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('title')
			->add('text')
			->add('private', null, [
				'required' => false,
			])
			->add('publishFrom')
			->add('publishTo')
			->add('tags', null, [
				'property' => 'title'
			])
			->add('files', 'collection', [
				'type' => new FileType(),
				'allow_add' => true,
				'prototype' => true,
				// Post update
				'by_reference' => false,
			])
			->add('submit', 'submit', [
				'attr' => [
					'class' => 'btn btn-primary'
				]
			]);
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Cvut\Fit\BiWt1\Blog\CommonBundle\Entity\Post'
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'cvut_fit_biwt1_blog_commonbundle_post';
	}
}
