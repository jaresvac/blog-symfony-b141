<?php
namespace Cvut\Fit\BiWt1\Blog\CommonBundle\Security;

use Cvut\Fit\BiWt1\Blog\CommonBundle\Entity\User;
use Cvut\Fit\BiWt1\Blog\CommonBundle\Entity\UserRepository;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserProvider implements UserProviderInterface, ContainerAwareInterface {

	/**
	 * @var ContainerInterface
	 */
	protected $container;

	/**
	 * Sets the Container.
	 *
	 * @param ContainerInterface|null $container A ContainerInterface instance or null
	 *
	 * @api
	 */
	public function setContainer(ContainerInterface $container = null)
	{
		$this->container = $container;
	}

	/**
	 * Loads the user for the given username.
	 *
	 * This method must throw UsernameNotFoundException if the user is not
	 * found.
	 *
	 * @param string $username The username
	 *
	 * @return UserInterface
	 *
	 * @see UsernameNotFoundException
	 *
	 * @throws UsernameNotFoundException if the user is not found
	 *
	 */
	public function loadUserByUsername($username) {
		if(!($user = $this->container->get('cvut_fit_biwt1_blog_common.repository.user')->findOneBy(array('name' => $username))))
			throw new UsernameNotFoundException(
				sprintf('Username "%s" does not exist.', $username)
			);
		return $user;
	}

	/**
	 * Refreshes the user for the account interface.
	 *
	 * It is up to the implementation to decide if the user data should be
	 * totally reloaded (e.g. from the database), or if the UserInterface
	 * object can just be merged into some internal array of users / identity
	 * map.
	 * @param UserInterface $user
	 *
	 * @return UserInterface
	 *
	 * @throws UnsupportedUserException if the account is not supported
	 */
	public function refreshUser(UserInterface $user) {
		if(!$user instanceof User) {
			throw new UnsupportedUserException(
				sprintf('Instances of "%s" are not supported.', get_class($user))
			);
		}

		return $this->loadUserByUsername($user->getUsername());
	}

	/**
	 * Whether this provider supports the given user class
	 *
	 * @param string $class
	 *
	 * @return bool
	 */
	public function supportsClass($class) {
		return $class === 'Cvut\Fit\BiWt1\Blog\CommonBundle\Entity\User';
	}

}