<?php
/**
 * Created by PhpStorm.
 * User: jares
 * Date: 06/01/15
 * Time: 13:35
 */

namespace Cvut\Fit\BiWt1\Blog\CommonBundle\Service;


use Cvut\Fit\BiWt1\Blog\CommonBundle\Entity\Comment;
use Cvut\Fit\BiWt1\Blog\CommonBundle\Entity\CommentInterface;
use Cvut\Fit\BiWt1\Blog\CommonBundle\Entity\File;
use Cvut\Fit\BiWt1\Blog\CommonBundle\Entity\FileInterface;
use Cvut\Fit\BiWt1\Blog\CommonBundle\Entity\Post;
use Cvut\Fit\BiWt1\Blog\CommonBundle\Entity\PostInterface;
use Cvut\Fit\BiWt1\Blog\CommonBundle\Entity\Tag;
use Cvut\Fit\BiWt1\Blog\CommonBundle\Entity\TagInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;

class BlogService implements BlogInterface
{
	/** @var \Doctrine\ORM\EntityRepository */
	protected $tags;

	/** @var \Doctrine\ORM\EntityRepository */
	protected $posts;

	/** @var \Doctrine\ORM\EntityRepository */
	protected $comments;

	/** @var \Doctrine\ORM\EntityRepository */
	protected $files;

	/**
	 * @param EntityManager $em
	 */
	public function __construct(EntityManager $em)
	{
		$this->tags = $em->getRepository(Tag::class);
		$this->posts = $em->getRepository(Post::class);
		$this->comments = $em->getRepository(Comment::class);
		$this->files = $em->getRepository(File::class);
	}

	/**
	 * Vytvori novy tag, pokud neexistuje
	 *
	 * @param TagInterface $tag
	 * @return TagInterface
	 */
	public function createTag(TagInterface $tag)
	{
		$this->tags->create($tag);
		return $tag;
	}

	/**
	 * Upravi stavajici tag
	 *
	 * @param TagInterface $tag
	 * @return TagInterface
	 */
	public function updateTag(TagInterface $tag)
	{
		$this->tags->update($tag);
		return $tag;
	}

	/**
	 * Smaze tag
	 *
	 * @param TagInterface $tag
	 * @return TagInterface
	 */
	public function deleteTag(TagInterface $tag)
	{
		$this->tags->remove($tag);
		return $tag;
	}

	/**
	 * Nalezne tag podle ID a vrati
	 *
	 * @param $id
	 * @return TagInterface
	 */
	public function findTag($id)
	{
		return $this->tags->find($id);
	}

	/**
	 * Najde a vrati tagy podle kriterii
	 *
	 * @param mixed $criteria - cast QueryBuilderu, ktera se pouzije v QueryBuilder::andWhere
	 * @return Collection<TagInterface>
	 */
	public function findTagBy($criteria)
	{
		return $this->tags->createQueryBuilder('e')
			->andWhere($criteria)
			->getQuery()
			->getResult();
	}

	/**
	 * Vytvori novy zapisek
	 *
	 * @param PostInterface $post
	 * @return PostInterface
	 */
	public function createPost(PostInterface $post)
	{
		$this->posts->create($post);
		return $post;
	}

	/**
	 * Aktualizuje zapisek
	 *
	 * @param PostInterface $post
	 * @return PostInterface
	 */
	public function updatePost(PostInterface $post)
	{
		$post->setModified(new \DateTime());
		$this->posts->update($post);
		return $post;
	}

	/**
	 * Smaze zapisek
	 *
	 * @param PostInterface $post
	 * @return PostInterface
	 */
	public function deletePost(PostInterface $post)
	{
		$this->posts->remove($post);
		return $post;
	}

	/**
	 * Najde zapisek podle ID a vrati
	 *
	 * @param $id
	 * @return PostInterface
	 */
	public function findPost($id)
	{
		return $this->posts->find($id);
	}

	/**
	 * Najde zapisky podle kriterii a vrati
	 *
	 * @param mixed $criteria - cast QueryBuilderu, ktera se pouzije v QueryBuilder::andWhere
	 * @return Collection<PostInterface>
	 */
	public function findPostBy($criteria)
	{
		return $this->posts->createQueryBuilder('e')
			->andWhere($criteria)
			->getQuery()
			->getResult();
	}

	/**
	 * Prida k zapisku komentar
	 *
	 * @param PostInterface $post
	 * @param CommentInterface $comment
	 * @param CommentInterface $parentComment
	 * @return PostInterface
	 */
	public function addComment(PostInterface $post, CommentInterface $comment,
							   CommentInterface $parentComment = null)
	{
		$comment->setParent($parentComment);
		$comment->setPost($post);
		$this->comments->create($comment);
		return $post;
	}

	/**
	 * Odebere od zapisku komentar
	 *
	 * @param CommentInterface $comment
	 * @return PostInterface
	 */
	public function deleteComment(CommentInterface $comment)
	{
		$this->comments->remove($comment);
		return $comment;
	}

	/**
	 * Prida k zapisku a pripadne komentari soubor
	 *
	 * @param $file
	 * @param $post
	 * @param $comment
	 * @return PostInterface
	 */
	public function addPostFile(FileInterface $file, PostInterface $post,
								CommentInterface $comment = null)
	{
		if ($comment) {
			$file->setComment($comment);
		}
		$file->setPost($post);
		$this->files->create($file);
		return $post;
	}

	/**
	 * Odebere od zapisku soubor
	 *
	 * @param $file
	 * @return PostInterface
	 */
	public function deleteFile($file)
	{
		$this->files->remove($file);
		return $file;
	}

}
