<?php

namespace Cvut\Fit\BiWt1\Blog\SecondBundle\Controller;

use Cvut\Fit\BiWt1\Blog\CommonBundle\Entity\Image;
use Cvut\Fit\BiWt1\Blog\CommonBundle\Entity\Post;
use Cvut\Fit\BiWt1\Blog\CommonBundle\Form\PostType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class PostController
 * @package Cvut\Fit\BiWt1\Blog\SecondBundle\Controller
 *
 * @Route("post")
 */
class PostController extends Controller
{
	/**
	 * @Route("/create", name="create_post")
	 * @Template()
	 */
	public function createAction()
	{
		$post = new Post();

		$form = $this->createForm(new PostType(), $post, [
			'action' => $this->generateUrl('save_post')
		]);

		return [
			'form' => $form->createView()
		];
	}

	/**
	 * @Route("/update/{id}", name="update_post")
	 * @Template()
	 */
	public function updateAction($id)
	{
		$post = $this->getDoctrine()
			->getRepository(Post::class)
			->find($id);

		if (!$post) {
			throw new NotFoundHttpException;
		}

		if (!$this->getUser()->isAdmin() && $post->getAuthor() !== $this->getUser()) {
			throw new AccessDeniedHttpException;
		}

		$form = $this->createForm(new PostType(), $post, [
			'action' => $this->generateUrl('save_post', [
				'id' => $post->getId()
			])
		]);

		return [
			'form' => $form->createView()
		];
	}


	/**
	 * @Route("/save/{id}", defaults={"id" = null}, name="save_post")
	 */
	public function saveAction($id = NULL, Request $request)
	{
		$em = $this->getDoctrine()->getManager();

		if ($id) {
			$post = $em->getRepository(Post::class)->find($id);
			if (!$post) {
				throw new NotFoundHttpException;
			}
			if (!$this->getUser()->isAdmin() && $post->getAuthor() !== $this->getUser()) {
				throw new AccessDeniedHttpException;
			}
		} else {
			$post = new Post();
			$post->setAuthor($this->getUser());
		}

		$form = $this->createForm(new PostType(), $post);

		$form->handleRequest($request);

		if ($form->isValid()) {
			$post = $form->getData();

			foreach ($post->getFiles() as $file) {
				if (is_string($file->getData()) && in_array($file->getInternetMediaType(), ['image/jpeg', 'image/png', 'image/gif'])) {
					$image = new Image();
					$image->setData($file->getData());
					$image->setName($file->getName());
					$image->setInternetMediaType($file->getInternetMediaType());
					$im = imagecreatefromstring($image->getData());
					$image->setDimensionX(imagesx($im));
					$image->setDimensionY(imagesy($im));
					$post->getFiles()->removeElement($file);
					$post->addFile($image);
				}
				$file->setPost($post);
			}

			if ($id) {
				$this->get('blog_service')->updatePost($post);
			} else {
				$this->get('blog_service')->createPost($post);
			}
			$em->flush();

			return $this->redirect($this->generateUrl('detail', ['id' => $post->getId()]));
		}

		return $this->render(
			$id ? 'Bundle:Post:update.html.twig' : 'Bundle:Post:create.html.twig',
			['form' => $form->createView()]
		);
	}


	/**
	 * @Route("/delete-post/{id}", name="delete_post")
	 */
	public function deletePostAction($id)
	{
		$post = $this->get('cvut_fit_biwt1_blog_common.repository.post')->find($id);
		if (!$post) {
			throw new NotFoundHttpException;
		}
		if ($this->getUser()->isAdmin() || $this->getUser() === $post->getAuthor()) {
			$this->get('blog_service')->deletePost($post);
			return $this->redirect($this->generateUrl('homepage'));
		}
		throw new AccessDeniedHttpException;
	}

}
