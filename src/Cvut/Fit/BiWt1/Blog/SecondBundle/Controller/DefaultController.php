<?php

namespace Cvut\Fit\BiWt1\Blog\SecondBundle\Controller;

use Cvut\Fit\BiWt1\Blog\CommonBundle\Entity\Comment;
use Cvut\Fit\BiWt1\Blog\CommonBundle\Entity\Image;
use Cvut\Fit\BiWt1\Blog\CommonBundle\Entity\Post;
use Cvut\Fit\BiWt1\Blog\CommonBundle\Form\CommentType;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultController extends Controller
{

	const POSTS_PER_PAGE = 8;
	const COMMENTS_PER_PAGE = 8;

	/**
	 * @Route("/{page}", defaults={"page" = 1}, name="homepage")
	 * @Template()
	 */
	public function indexAction($page)
	{
		if ($page < 1) {
			$this->redirect('homepage', ['page' => 1]);
		}

		$qb = $this->getDoctrine()
			->getRepository(Post::class)
			->createQueryBuilder('e')
			->where('e.publishFrom < :now AND e.publishTo > :now')
			->orderBy('e.created', 'DESC')
			->setParameter('now', new \DateTime());

		if (!$this->getUser()) {
			$qb->andWhere('e.private = 0');
		}

		$paginator = new Paginator($qb);

		$paginator->getQuery()->setMaxResults(self::POSTS_PER_PAGE);
		$paginator->getQuery()->setFirstResult(($page - 1) * self::POSTS_PER_PAGE);


		return [
			'posts' => $paginator,
			'count' => max(ceil(count($paginator) / self::POSTS_PER_PAGE), 1),
			'page' => $page,
		];
	}


	/**
	 * @Route("/detail/{id}/{page}", defaults={"page" = 1}, name="detail")
	 * @Template()
	 */
	public function detailAction($id, $page, Request $request)
	{
		$post = $this->getDoctrine()
			->getRepository(Post::class)
			->find(intval($id));

		if (!$post) {
			throw new NotFoundHttpException;
		}

		if ($post->getPrivate() && !$this->getUser()) {
			throw new AccessDeniedHttpException;
		}

		$comments = $post->getCommentsBy(
			Criteria::create()
				->setFirstResult(($page - 1) * self::COMMENTS_PER_PAGE)
				->setMaxResults(self::COMMENTS_PER_PAGE)
				->orderBy(['created' => 'DESC'])
		);

		$commentId = $request->query->get('commentId');
		if ($commentId) {
			$comment = $this->get('cvut_fit_biwt1_blog_common.repository.comment')->find($commentId);
			if (!$comment) {
				throw new NotFoundHttpException;
			}
		} else {
			$comment = new Comment();
		}

		if ($this->getUser()) {
			$form = $this->createForm(new CommentType(), $comment, [
				'action' => $this->generateUrl('create_comment',
					[
						'post' => $post->getId(),
						'commentId' => $commentId,
						'parentId' => $request->query->get('parent')
					])
			])->createView();
		} else {
			$form = NULL;
		}

		return [
			'post' => $post,
			'count' => max(ceil(count($post->getComments()) / self::COMMENTS_PER_PAGE), 1),
			'page' => $page,
			'comments' => $comments,
			'form' => $form
		];
	}


	/**
	 * @Route("/create-comment/{post}/{page}", defaults={"page" = 1}, name="create_comment")
	 * @Template("Bundle:Default:detail.html.twig")
	 */
	public function saveCommentAction($post, $page, Request $request)
	{
		$commentId = $request->query->get('commentId');
		if ($commentId) {
			$comment = $this->get('cvut_fit_biwt1_blog_common.repository.comment')->find($commentId);
			if (!$comment) {
				throw new NotFoundHttpException;
			}
		} else {
			$comment = new Comment();
		}

		$post = $this->get('cvut_fit_biwt1_blog_common.repository.post')->find($post);
		if (!$post) {
			throw new NotFoundHttpException;
		}

		$form = $this->createForm(new CommentType(), $comment, [
			'action' => $this->generateUrl('create_comment', ['post' => $post->getId()])
		]);

		$parent = NULL;
		$parent = $parent ? $this->get('cvut_fit_biwt1_blog_common.repository.comment')->find($parent) : NULL;

		$form->handleRequest($request);

		if ($form->isValid()) {
			$comment = $form->getData();

			foreach ($comment->getFiles() as $file) {
				if (is_string($file->getData()) && in_array($file->getInternetMediaType(), ['image/jpeg', 'image/png', 'image/gif'])) {
					$image = new Image();
					$image->setData($file->getData());
					$image->setName($file->getName());
					$image->setInternetMediaType($file->getInternetMediaType());
					$im = imagecreatefromstring($image->getData());
					$image->setDimensionX(imagesx($im));
					$image->setDimensionY(imagesy($im));
					$comment->getFiles()->removeElement($file);
					$comment->addFile($image);
					$image->setPost($post);
				} else {
					$comment->addFile($file);
					$file->setPost($post);
				}
			}

			$comment->setAuthor($this->getUser());
			$this->get('blog_service')->addComment($post, $comment, $parent);
			$this->getDoctrine()->getEntityManager()->flush();
			return $this->redirect($this->generateUrl('detail', ['id' => $post->getId()]));
		}


		$comments = $post->getCommentsBy(
			Criteria::create()
				->setFirstResult(($page - 1) * self::COMMENTS_PER_PAGE)
				->setMaxResults(self::COMMENTS_PER_PAGE)
		);

		return [
			'post' => $post,
			'count' => max(ceil(count($post->getComments()) / self::COMMENTS_PER_PAGE), 1),
			'page' => $page,
			'comments' => $comments,
			'form' => $form->createView()
		];
	}


	/**
	 * @Route("/attachment/{id}", name="attachment")
	 */
	public function attachmentAction($id)
	{

		$file = $this->get('cvut_fit_biwt1_blog_common.repository.file')->find($id);

		if (!$file) {
			throw new NotFoundHttpException;
		}

		if ($file->getPost()->getPrivate() && !$this->getUser()) {
			throw new AccessDeniedHttpException;
		}

		$response = new StreamedResponse(function () use ($file) {
			echo stream_get_contents($file->getData());
		});

		$response->headers->set('Content-Type', $file->getInternetMediaType());
		return $response;
	}


	/**
	 * @Route("/delete-comment/{post}/{id}", name="comment_delete")
	 */
	public function deleteCommentAction($post, $id)
	{

		$blog = $this->get('blog_service');

		$comment = $this->get('cvut_fit_biwt1_blog_common.repository.comment')->find($id);

		if (!$comment) {
			throw new NotFoundHttpException;
		}
		if ($this->getUser()->isAdmin() || $comment->getAuthor() === $this->getUser()) {
			$blog->deleteComment($comment);
			return $this->redirect($this->generateUrl('detail', ['id' => $post]));
		} else {
			throw new AccessDeniedHttpException;
		}
	}
}
