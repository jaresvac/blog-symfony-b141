<?php

namespace Cvut\Fit\BiWt1\Blog\SecondBundle\Controller;

use Cvut\Fit\BiWt1\Blog\CommonBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * Class SignController
 * @package Cvut\Fit\BiWt1\Blog\SecondBundle\Controller
 *
 * @Route("sign")
 */
class SignController extends Controller
{
	/**
	 * @Route("/in", name="login")
	 * @Template()
	 */
	public function inAction()
	{
		$request = $this->getRequest();
		$session = $request->getSession();

		// get the login error if there is one
		if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
			$error = $request->attributes->get(
				SecurityContext::AUTHENTICATION_ERROR
			);
		} else {
			$error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
			$session->remove(SecurityContext::AUTHENTICATION_ERROR);
		}


		return array(
			'last_username' => $request->getSession()->get(SecurityContext::LAST_USERNAME),
			'error' => $error
		);
	}

	/**
	 * @Route("/check", name="login_check")
	 */
	public function loginCheckAction()
	{

		$this->redirect('/');
	}


	/**
	 * @Route("/pass/{password}")
	 */
	public function passAction($password)
	{
		$factory = $this->get('security.encoder_factory');
		$user = new User();

		$encoder = $factory->getEncoder($user);
		$password = $encoder->encodePassword($password, $user->getSalt());
		echo $password;
		exit;
	}


	/**
	 * @Route("/out", name="logout")
	 */
	public function outAction()
	{

	}

}
